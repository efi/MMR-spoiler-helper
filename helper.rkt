#lang racket

(require threading)
(require racket/hash)

(define debug (make-parameter #f))

(define first-line #px"^\\s*Location.*")
(define last-line  #px"^\\s*Gossip Stone\\s+Message.*")
(define arrow-line #px".*->.*")

(define requirements (hash))

(define (add-requirements file)
  (set! requirements
    (hash-union
      requirements
      (if (file-exists? (build-path "logic" file))
        (apply hash (file->list (build-path "logic" file)))
        (raise-user-error (format "Required file \"~a\" in \"logic\" directory could not be found!!" file)))
      #:combine (λ (a b) b))))

(define (get-stars l)
  (hash
    'location   (first l)
    'item       (first (regexp-match #px"[^*]+" (second l)))
    'importance (string-length (first (regexp-match #px"\\**$" (second l))))))

(define (map-hash-key k l)
  (map (λ (i) (hash-ref i k)) l))

(define (hash-key-filter f k l)
  (filter (λ (i) (f (hash-ref i k))) l))

(define (eat-file file)
  (~> (file->lines file)
      (dropf (λ (l) (not (regexp-match? first-line l))))
      (takef (λ (l) (not (regexp-match? last-line l))))
      (~>> (filter (λ (l) (regexp-match? arrow-line l)))
           (map (λ (l) (regexp-split #px"\\s+->\\s+" l)))
           (map get-stars))))

(define (is-check? s) (hash-has-key? requirements s))

(define (is-attainable? held check)
  (define is-held? (curry set-member? held))
  (if (is-check? check)
    (or
      (empty? (hash-ref requirements check))
      (ormap
        (λ (opt)
           (cond
             [(list? opt) (andmap (λ (c) (is-attainable? held c)) opt)]
             [(is-check? opt) (is-attainable? held opt)]
             [else (is-held? opt)]))
        (hash-ref requirements check)))
    (is-held? check)))

(define (attainable-list held)
  (list->set
    (filter
      (λ (r) (is-attainable? held r))
      (hash-keys requirements))))

(define (printset s port)
  (set-map s (λ (l) (displayln (format "» ~a" l) port))))

(define (help-instruction depth item port)
  (displayln (format "[~a] Obtain ~a from ~a." depth (hash-ref item 'item) (hash-ref item 'location)) port))

(define (provide-solution file item-locations)
  (let ([port (open-output-file file #:exists 'truncate)]
        [item-names (list->set (map-hash-key 'item item-locations))]
        ;[relevant (filter (λ (i) (> (hash-ref i 'importance) 0)) item-locations)]
        [mandatory (list->set (map-hash-key 'item (hash-key-filter (λ (i) (= i 2)) 'importance item-locations)))])
    (define (item-at location [possible item-locations])
      (let ([imatch (findf (λ (i) (string=? location (hash-ref i 'location))) possible)])
        (when imatch (hash-ref imatch 'item))))
    (define (location-of item [possible item-locations])
      (let ([lmatch (findf (λ (i) (string=? item (hash-ref i 'item))) possible)])
        (when lmatch (hash-ref lmatch 'location))))
    (when (debug)
      (displayln (format "Debug is: ~a" (debug)) port)
      (displayln "Mandatory:" port)
      (set-map mandatory (λ (l) (displayln l port))))
    (let loop ([held (set)] [checked (set)] [depth 0])
      (let ([attainable (attainable-list held)])
        (when (debug)
          (displayln "---" port)
          (displayln "Having:" port)
          (printset held port)
          ;(writeln checked port)
          (displayln "Can get:" port)
          (printset (set-subtract attainable checked) port)
          (displayln "Will get:" port)
          (printset (~> attainable
                        (set-subtract checked)
                        (set-map item-at)
                        (list->set)
                        (set-subtract held)
                        (set-remove (void)))
                    port))
        (when (set-empty? (set-subtract attainable checked)) (raise-user-error "Found a deadlock!!"))
        (when (set-empty? (~> attainable
                              (set-subtract checked)
                              (set-map item-at)
                              (list->set)
                              (set-subtract held)
                              (set-remove (void))))
          (raise-user-error "Found a deadlock!!"))
        (for/set
          ([n (~> attainable
                  (set-subtract checked)
                  (set-map item-at)
                  (list->set)
                  (set-subtract held)
                  (set-intersect mandatory)
                  (set-remove (void)))])
          (help-instruction
            depth
            (findf
              (λ (i) (string=? n (hash-ref i 'item)))
              (filter
                (λ (i) (is-attainable? held (hash-ref i 'location)))
                item-locations))
            port))
        (unless (set-empty? (set-subtract mandatory held))
          (loop
            (~> attainable
                (set-map item-at)
                (list->set)
                (set-union held)
                (set-remove (void)))
            (~> attainable
                (set-map item-at)
                (list->set)
                (set-union checked)
                (set-remove (void)))
            (add1 depth)))))
    (displayln "VICTORY!!" port)
    (close-output-port port)))

(define (main files)
  (if (file-exists? (first files))
    (provide-solution (second files) (eat-file (first files)))
    (displayln (string-append "Error: Couldn't find spoiler file " (first files)))))

(add-requirements "default.logic")
(main (command-line
        #:program "Majora's Mask Randomizer Solver"
        #:multi
          [("-l" "--logic") file "Add a logic file to the ruleset"
                            (add-requirements file)]
        #:once-each
          [("-d" "--debug") "Show ALL debug information"
                            (debug #t)]
        #:args (spoiler-file [solution-file (string-append spoiler-file ".solution")])
        (list spoiler-file solution-file)))
