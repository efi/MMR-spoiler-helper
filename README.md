<img src="https://awoo.systems/trans_rights_badge.svg"/><br>
# MMR-spoiler-helper

A helper program to make reading Majora's Mask Randomizer spoiler logs easier by stripping out the extras.
Feed it any spoiler file and it will find the items you need and give you an aproximate route of the minimum steps you need to take.
If you get a deadlock it means the route can't be found with the current logic settings, so make sure it's configured the same way the randomizer was when making the rom.

## How to use it

Usually you just need to run the program from the command line with the spoiler file you want to solve.

```
$ helper majora.spoiler
```

This will generate a `majora.spoiler.solution` file you can open with any text editor, just like the spoiler file.
Each step in the solution file has a number before it. Two steps with the same number may be performed in any order, there is no guarantee that it will be faster. In fact, sometimes it might be faster to get some later items first to make other checks easier. The order just means what is **possible** to get at each step, not what is optimal for a speedrun.

You can change the name of the file created by giving it after the input file.

```
$ helper majora.spoiler solution.txt
```

This will create a file named `solution.txt` instead of a .solution file. Windows & graphical desktop users may use this for the default action on .txt files when double-clicking. It is also nice if you're uploading it to a Discord channel.

## Logic files

By default, the solver uses the `default.logic` file in the `logic` folder. You don't need to change this file. You can just make a new set of rules and use `-l` to make it rewrite the matching rules. Some useful files are provided. For example.

```
$ helper -l open-boss-doors.logic majora.spoiler
```

This will look for the `open-boss-doors.logic` file **inside** the `logic` folder and add those rules to the set. This particular logic file rewrites the requirements for defeating the four bosses by removing the "Boss Key" requirements, as happens when you use some keysanity settings.

The other provided files are:
- `bomber-code-skip.logic` for when backflipping over the bomber kid blocking the way to the obsrvatory is allowed.
- `goron-jump.logic` for when jumping over epona fences with the goron mask and a bomb is allowed.
- `ikana-ice-skip.logic` for when using the hookshot to skip freezing the octorocks in Ikana river is allowed.
- `lensless.logic` for when getting the heart piece on the pillar on the way to Snowhead without the Lens of Truth is allowed.
- `sword-start.logic` for when we always start with the Kokiri Sword.

When I say "is allowed" you may read it as "it might be mandatory to do so". This means it's allowed in-logic, for the randomizer.

### Writing logic files

The format for logic files is very simple. Start each line with the **literal check name** as it appears in the spoiler log, between quotation marks. You can add checks, but you can't remove them. Removing checks should not ever be necessary anyway.
You can use a name that doesn't appear in the spoiler log as long as it contains checks that do.
Then follow it with a `[]` delimited list of alternatives. (Technically you can use `()` or `{}`, but square looks better imo)
If you put elements inside another bracket pair they will be all required instead of alternative.

Example form the default logic:

```
"Honey and Darling Any Day"  ["Bombs" "Bow" ["Deku Mask" "Magic"]]
```

This means the check for Honey and Darling's heart piece, which can be obtained any day, would require either bombs, for the bombchu and bomb basket minigames, a bow or both the deku mask and magic for the target shooting minigame.
Bombs, Bow and Magic are defined at the top of the default logic file as any of the items that give you access to those abilities.
If you need more complex logic, you can make deeper nesting by using that kind of abstract check name.
For example, look at the "Snowhead Icicle Room Chest" in the default logic file.

```
"Snowhead Icicle Room Chest"  [["Snowhead Access" "SH Icicle Room"]]
"SH Icicle Room"              [["SH Icicle Room A" "SH Icicle Room B"]]
"SH Icicle Room A"            ["Snowhead Small Key" "Explosives"]
"SH Icicle Room B"            ["Hookshot" "Fire Shot"]
```

You need to be able to get to Snowhead Temple, and in addition you need both of one of each A and B sets. Depending on wether you're doing it forward or backwards.

Also, remember that any added logic file will replace any previous entry of a check with the same name.

## Other things

You can use the -d flag to get A LOT more information on what the solver is trying to do, but this is only useful if you are getting a deadlock and you don't know what logic is blocking the progress. It won't tell you what blocked it, but it will tell you exactly what could be checked at every round of attempts. If this differs on what you know can be obtained, then you can add a rule that tells the solver when it can get what and it will follow through.

I tried to make the default logic accurate to the current (2021) MMR race settings, but I am only human.

If you want to run the scripts in this repo yourself, you will need racket and the threading racket package installed. You can run it as `$ racket helper.rkt majora.solution` from the source.

Otherwise there should be Windows and Linux binaries of the latest build in the releases section of this website https://git.lubar.me/efi/MMR-spoiler-helper .

If you've obtained these files from somewhere else, that's weird, yo, what happened?
Other people are allowed to modify and redistribute the software, but why would they? I just don't know.
License in the license file, but tl;dr it's MIT.

Anyway, if you really need to contact me and you can't find me on the MMR Discord and don't want to use the git site, I'm at https://chitter.xyz/@efi . All questions welcome.

